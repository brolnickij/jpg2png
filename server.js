const fs = require('nano-fs');
const clearFolder = require('empty-folder');
const gm = require('gm');


const jpgFolder = './images/jpg';
const convertedFilesFolder = './images/png';


fs.listFiles(jpgFolder)
  .then(list => {
    for (let fileName of list) {
      if (fileName === '.DS_Store') continue;

      const fileNameOutFormat = fileName.replace(/(.jpg|.png)/gi, '');

      gm(`${jpgFolder}/${fileName}`)
        .quality(100)
        .write(`${convertedFilesFolder}/${fileNameOutFormat}.png`, err => {
          if (!err) console.log(`Convert ${fileName} to ${fileNameOutFormat}.png`);
          else console.log(err);
        });
    }
  })
  .then(() => {
    clearFolder(`${jpgFolder}`, false, () => console.log(`${jpgFolder} is clear`));
  })
  .catch(err => console.log(err));
